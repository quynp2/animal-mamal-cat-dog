package com.devcamp.javabasic_j01.s10;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    public void greets() {
        System.out.println("Moew");
    }

    @Override
    public String toString() {
        return "Cat [Mamal][Animal][name=" + name;
    }

}
