package com.devcamp.javabasic_j01.s10;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    public void greets() {
        System.out.println("Woof");
    }

    public void greets(Dog another) {
        System.out.println("Wooooooof");
    }

    @Override
    public String toString() {
        return "Dog[Mamal][Animal][Name=" + name + "]";
    }

}
