package com.devcamp.javabasic_j01.s10;

public class Mamal extends Animal {

    public Mamal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Mamal[Animal][name= " + name + "]";
    }

}
