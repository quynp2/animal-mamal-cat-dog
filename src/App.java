
import com.devcamp.javabasic_j01.s10.Animal;
import com.devcamp.javabasic_j01.s10.Cat;
import com.devcamp.javabasic_j01.s10.Dog;
import com.devcamp.javabasic_j01.s10.Mamal;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Animal animal1 = new Animal("Trau");
        Animal animal2 = new Animal("Bo");
        System.out.println(animal1);
        System.out.println(animal2);

        Mamal mamal1 = new Mamal("Heo");
        Mamal mamal2 = new Mamal("Lon");
        System.out.println(mamal1);
        System.out.println(mamal2);

        Cat cat1 = new Cat("Tam The");
        Cat cat2 = new Cat("Hac miu");
        System.out.println(cat1);
        System.out.println(cat2);
        cat1.greets();

        Dog dog1 = new Dog("Cho con");
        Dog dog2 = new Dog("Cho bu");
        System.out.println(dog1);
        System.out.println(dog2);
        dog1.greets();
        dog1.greets(dog2);

    }
}
